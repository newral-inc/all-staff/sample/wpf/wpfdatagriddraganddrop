﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// [WPF]DataGridをクリックしたセルの位置を調べる
// https://mohmongar.net/?p=1226

// 【C# WPF】GridコントロールのRowIndexおよびColumnIndexについて
// https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q1443661384

// WPF Tutorial | Drag & Drop
// https://sites.google.com/site/toriaezunomemo/home/visual-studiomemo/wpf-tutorial-drag-drop

// [XAML/C#] WPF におけるドラッグ アンド ドロップ (Windows フォームから WPF へ)
// https://code.msdn.microsoft.com/windowsdesktop/XAMLCVB-WPF-Windows-WPF-a1c048ae

// Q039. DataGrid で選択行をドラッグドロップで移動するにはどうすればいいのですか？
// http://d.hatena.ne.jp/hilapon/20110209/1297247754

namespace WPFDataGridDragAndDrop
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // DataGrid1を設定します。
            dataGrid1.Columns.Add(new DataGridTextColumn() { Header = "番号", Binding = new Binding("no") });
            dataGrid1.Columns.Add(new DataGridTextColumn() { Header = "値", Binding = new Binding("value") });
            var dataTable1 = new DataTable("DataTable1");
            dataTable1.Columns.Add(new DataColumn("no", typeof(string)));
            dataTable1.Columns.Add(new DataColumn("value", typeof(string)));
            for (int i = 0; i < 5; i++)
            {
                var dataRow1 = dataTable1.NewRow();
                dataRow1["no"] = i.ToString();
                dataRow1["value"] = i.ToString();
                dataTable1.Rows.Add(dataRow1);
            }
            dataGrid1.DataContext = dataTable1;

            // DataGrid2を設定します。
            dataGrid2.Columns.Add(new DataGridTextColumn() { Header = "番号", Binding = new Binding("no") });
            dataGrid2.Columns.Add(new DataGridTextColumn() { Header = "値", Binding = new Binding("value") });
            var dataTable2 = new DataTable("DataTable2");
            dataTable2.Columns.Add(new DataColumn("no", typeof(string)));
            dataTable2.Columns.Add(new DataColumn("value", typeof(string)));
            for (int i = 0; i < 5; i++)
            {
                var dataRow2 = dataTable2.NewRow();
                dataRow2["no"] = (i + 10).ToString();
                dataRow2["value"] = (i + 10).ToString();
                dataTable2.Rows.Add(dataRow2);
            }
            dataGrid2.DataContext = dataTable2;
        }

        /**
         * @brief DataGrid1でマウスがダブルクリックされた時に呼び出されます。
         * 
         * @param [in] sender DataGrid1
         * @param [in] e マウスボタンイベント
         */
        private void DataGrid1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // 選択されたDataGrid1のセルの列番号、行番号を取得します。
            var dataGrid = sender as DataGrid;
            var columnIndex = dataGrid.CurrentCell.Column.DisplayIndex;
            var rowIndex = dataGrid.Items.IndexOf(dataGrid1.CurrentItem);

            // 選択されたセルを取得します。
            var row = dataGrid1.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            var cell = dataGrid.Columns[columnIndex].GetCellContent(row);
            var data = cell as TextBlock;

            // ドラッグアンドドロップ動作を開始します。
            DragDrop.DoDragDrop(dataGrid, data, DragDropEffects.Copy);
        }

        /**
         * @brief DataGrid2にドラッグが開始された時に呼び出されます。
         * 
         * @param [in] sender DataGrid2
         * @param [in] e マウスボタンイベント
         */
        private void DataGrid2_DragEnter(object sender, DragEventArgs e)
        {
            // ドラッグアンドドロップの効果をコピーとします。
            e.Effects = DragDropEffects.Copy;
        }

        /**
         * @brief DataGrid2にドロップされた時に呼び出されます。
         * 
         * @param [in] sender DataGrid2
         * @param [in] e マウスボタンイベント
         */
        private void DataGrid2_Drop(object sender, DragEventArgs e)
        {
            // 元データを取得します。
            var srcTextBlock = e.Data.GetData(typeof(TextBlock)) as TextBlock;

            // DataGrid2でDropされた位置のセル(TextBlock)を取得し、元データに書き換えます。
            var point = e.GetPosition((UIElement)sender);
            var hitResultTest = VisualTreeHelper.HitTest(dataGrid2, point);
            if (hitResultTest != null)
            {
                var visualHit = hitResultTest.VisualHit;
                while (visualHit != null)
                {
                    if (visualHit is TextBlock)
                    {
                        var dstTextBlock = visualHit as TextBlock;
                        dstTextBlock.Text = srcTextBlock.Text;
                        break;
                    }
                }
                visualHit = VisualTreeHelper.GetParent(visualHit);
            }
        }
    }
}
